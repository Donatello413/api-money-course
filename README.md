Как развернуть
1. Клонируйте репозиторий
2. cd money
3. cp .env.example .env
4. docker-compose up -d
5. docker exec -it money-php /bin/bash
6. composer install
7. php artisan key:generate
8. php artisan migrate && php artisan db:seed
9. Скопируйте из консоли сгенерированный api_key
10. Проект будет доступен по адресу `http://localhost:8000`
11. Для автоматического заполнения таблицы с курсами валют добавьте в кронтаб `* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1` где `path-to-your-project` путь до проекта

Использование
Для авторизации необходимо передавать в заголовке Authorization поулченный api_key

Доступны два метода:

1. GET /api/courses/{code} - текущий курс валюты
2. GET /api/courses/{code}/history - история курса валюты. Есть два необязтельных фильтра dateFrom dateTo
