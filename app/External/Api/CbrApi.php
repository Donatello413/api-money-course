<?php

namespace App\External\Api;

use GuzzleHttp\Client;

class CbrApi
{
    protected Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCurrentCourse(): array
    {
        $response = $this->client->get('https://www.cbr-xml-daily.ru/daily_json.js');

        return json_decode($response->getBody()->getContents(), true);
    }
}
