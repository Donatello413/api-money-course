<?php

namespace App\Console\Commands;

use App\External\Api\CbrApi;
use App\Models\CurrencyCourse;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-course';

    private CbrApi $apiClient;

    public function __construct(CbrApi $apiClient)
    {
        parent::__construct();
        $this->apiClient = $apiClient;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $todayCourses = $this->apiClient->getCurrentCourse();
        $today = Carbon::now()->toDateString();

        foreach ($todayCourses['Valute'] as $code => $currencyInfo) {
            $currencyCourse = CurrencyCourse::where('code', $code)->where('date', $today)->first();
            if (!$currencyCourse) {
                $currencyCourse = new CurrencyCourse();
                $currencyCourse->code = $code;
                $currencyCourse->date = $today;
            }
            $currencyCourse->value = $currencyInfo['Value'];
            $currencyCourse->nominal = $currencyInfo['Nominal'];
            $currencyCourse->save();
        }
    }
}
