<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyCourse extends Model
{
    public $timestamps = false;

    protected $table = 'currencies_courses';
}
