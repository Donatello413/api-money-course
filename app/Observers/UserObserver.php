<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Str;

class UserObserver
{
    /**
     * @param User $user
     * @return void
     */
    public function creating(User $user): void
    {
        $user->api_key = $this->generateApiKey();
    }

    /**
     * @return string
     */
    private function generateApiKey(): string
    {
        $apiKey = Str::random(50);

        if (!User::where('api_key', $apiKey)->exists()) {
            return $apiKey;
        }

        return $this->generateApiKey();
    }
}
