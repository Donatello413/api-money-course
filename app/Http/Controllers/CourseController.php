<?php

namespace App\Http\Controllers;

use App\Http\Requests\CurrencyHistoryRequest;
use App\Http\Resources\CurrencyCodeCollection;
use App\Http\Resources\CurrencyCodeResource;
use App\Models\CurrencyCourse;

class CourseController extends Controller
{
    /**
     * @param string $code
     * @return CurrencyCodeResource
     */
    public function getCourse(string $code): CurrencyCodeResource
    {
        $course = CurrencyCourse::where('code', $code)->orderBy('date', 'desc')->firstOrFail();

        return new CurrencyCodeResource($course);
    }

    /**
     * @param string $code
     * @param CurrencyHistoryRequest $request
     * @return CurrencyCodeCollection
     */
    public function getCourseHistory(string $code, CurrencyHistoryRequest $request): CurrencyCodeCollection
    {
        $data = $request->validated();

        $query = CurrencyCourse::where('code', $code);

        if (isset($data['dateFrom'])) {
            $query->where('date', '>=', $data['dateFrom']);
        }
        if (isset($data['dateTo'])) {
            $query->where('date', '<=', $data['dateTo']);
        }

        $courses = $query->orderBy('date', 'desc')->get();

        return new CurrencyCodeCollection($courses);
    }
}
