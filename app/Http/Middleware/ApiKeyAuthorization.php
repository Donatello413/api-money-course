<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ApiKeyAuthorization
{
    /**
     * Обработать входящий запрос.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $apiKey = $request->header('Authorization');

        if (!$apiKey) {
            return redirect(RouteServiceProvider::HOME);
        }

        $user = User::where('api_key', $apiKey)->first();

        if (!$user) {
            return redirect(RouteServiceProvider::HOME);
        }

        Auth::login($user);

        return $next($request);
    }
}
